What?
Remote Discourse theme for Friedrich Hospitality Foundation Community.

How to import this theme to live Discourse Forum
We use private repository. We must add SSH key for just first import our theme to any Discourse setup.

Go to Customize tab in Discourse admin panel.
Click to import button.
Select from the web option.
git@github.com:FHFworld/discourse.git use that value.
Select "Theme is in a private git repository".
Add SSH key to Deploy Keys under settings.
Add SSH key and save it.
Click the import button.
Example of File Structure
These files associated with the customize tabs in the admin panel. Excluding files such as: about.json, settings.yml.

about.json

assets/font.woff2
assets/background.jpg
assets/icon.svg

common/common.scss
common/head_tag.html
common/header.html
common/after_header.html
common/body_tag.html
common/footer.html
common/embedded.scss

desktop/desktop.scss
desktop/head_tag.html
desktop/header.html
desktop/after_header.html
desktop/body_tag.html
desktop/footer.html

mobile/mobile.scss
mobile/head_tag.html
mobile/header.html
mobile/after_header.html
mobile/body_tag.html
mobile/footer.html

settings.yml
Discourse Theme Color Variables
If you can make any color customization on Discourse you can do it with Discourse's color variables. There are more Discourse SCSS variables, but they are basic ones.

$primary
$secondary
$tertiary
$quaternary
$header_background
$header_primary
$highlight
$danger
$success
$love
About settings.yml
Themes can also include settings. The settings live in a settings.yml file that also live at the root of the theme directory. A theme settings file looks like this:

whitelisted_fruits:
  default: apples|oranges
  type: list

favorite_fruit:
  default: orange
  type: enum
  choices:
    - apple
    - banana